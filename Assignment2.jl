### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 15f8b5cb-ffc3-4520-b3d1-6f2d8d687f81
import Pkg; using Pkg;

# ╔═╡ 5d060036-20d8-4eb5-b2f0-82d3afbf1433
Pkg.activate("Project.toml")

# ╔═╡ 1b2d7df8-9f85-4d9e-a83c-b2eabed0be9b
Pkg.add("Flux")

# ╔═╡ 32248514-0f6d-4dba-ad09-715ab8a1d34c
Pkg.add("Images")

# ╔═╡ 2b175cd8-a5f9-47e8-86d9-92a0a91bbec1
Pkg.add("ImageFiltering");using ImageFiltering

# ╔═╡ 61b84f5c-64cd-463a-8d2c-e28dd9c7629f
Pkg.add("FileIO")

# ╔═╡ 8eaef5a1-59ab-4fe4-8dd6-961582346155
Pkg.add("ImageMagick")

# ╔═╡ b3216138-30be-4ca7-8d22-0430dbba195b
Pkg.add("ImageIO")

# ╔═╡ adc0d715-3d6d-4bc3-97e2-938ec2aed640
Pkg.add("TestImages")

# ╔═╡ 140619df-cd75-48bd-8bf1-01eab51e1a6e
Pkg.add("ImageView")

# ╔═╡ 84757d04-ac8c-4f49-9304-0eff91a71ee6
using Base

# ╔═╡ aeb62711-882c-4965-9e69-ea0f16f137a8
using PlutoUI

# ╔═╡ a59ac236-0851-4824-a403-7b5484320975
using DataFrames

# ╔═╡ 4bc3f7eb-68b9-466a-b348-1787cba409fd
using Flux

# ╔═╡ 167dab5f-c7b5-4048-b773-ccd55eaa0a9e
using Images

# ╔═╡ 4e7da1ce-2fe2-4f46-8adc-0fd5af961c02
using Plots

# ╔═╡ a2a4301d-e54b-43a2-ab46-a1bfde2b6008
using CSV

# ╔═╡ 6dd0a5ad-3e11-4cf9-b17d-c2ebed5b669a
using FileIO

# ╔═╡ 29fb15c9-38fb-4c29-b035-2b44bf8a86ad
using TestImages

# ╔═╡ ef9eb368-b92e-4950-8443-3360ddd48a94
using Flux.Data.MNIST, Statistics;


# ╔═╡ 7ba7cb1e-cf66-11eb-1b2e-51f8c0053504
md"# AIG Assignment 2"

# ╔═╡ fd565058-033c-4f97-85c9-ac48bf4d4dcb
md"###### Alfred Immanuel - 219074259"

# ╔═╡ d780b2f1-2403-4af6-a529-272179a22d89
normal1 = "C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0115-0001.jpeg"

# ╔═╡ 386755fc-61c4-4af4-b5d1-c027f2986b9d
normal2="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0117-0001.jpeg"

# ╔═╡ e91d905e-fc5c-46b2-a2ca-8c6726345b9f
normal3="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0119-0001.jpeg"

# ╔═╡ e5d366fb-30d4-409a-8b6a-210f95f9e627
normal4="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0122-0001.jpeg"

# ╔═╡ 77774475-2edf-47ad-b3ed-11ea6c25a2f2
normal5="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0125-0001.jpeg"

# ╔═╡ d388cae5-60c2-4c30-b1d7-afcc28d53952
normal6="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/IM-0127-0001.jpeg"

# ╔═╡ e3d7a8ce-8dac-45ea-a65f-82bd1dcc3133
img = load(normal6)

# ╔═╡ 7ced223b-d395-40f9-9c16-7d3df23213fe
load(normal5)

# ╔═╡ 576af1ce-70a1-4e9f-acdc-786dad1dbd92
pneumonia1 ="C:/Users/mpeswadmin/Downloads/chest_xray/train//PNEUMONIA/person1_bacteria_1.jpeg"

# ╔═╡ fddff671-ec45-43c3-9c31-4a7f7817f305
pneumonia2="C:/Users/mpeswadmin/Downloads/chest_xray/train/PNEUMONIA/person1_bacteria_2.jpeg"

# ╔═╡ 53550261-e47d-4582-a1ba-e389895ed929
pneumonia3="C:/Users/mpeswadmin/Downloads/chest_xray/train//PNEUMONIA/person2_bacteria_3.jpeg"

# ╔═╡ 7c46d9ba-7866-44bf-b1bc-7d9e71941a63
pneumonia4="C:/Users/mpeswadmin/Downloads/chest_xray/train//PNEUMONIA/person2_bacteria_4.jpeg"

# ╔═╡ df0f6347-c4f2-441a-8c38-e71b856eae15
pneumonia5="C:/Users/mpeswadmin/Downloads/chest_xray/train//PNEUMONIA/person3_bacteria_10.jpeg"

# ╔═╡ 8d880c29-f705-46cd-a32f-99efa51ad630
pneumonia6="C:/Users/mpeswadmin/Downloads/chest_xray/train//PNEUMONIA/person1_bacteria_11.jpeg"

# ╔═╡ 3a6a4ce5-78a9-46df-b083-cabae8787922
img1=load(pneumonia1)

# ╔═╡ 862d14f9-c307-4164-b594-1f7e59cd17ac
Path_to_Normal ="C:/Users/mpeswadmin/Downloads/chest_xray/train/NORMAL/"

# ╔═╡ 621a03ff-0dba-4072-86db-bd492626c758
images1 = readdir(Path_to_Normal)

# ╔═╡ d5e9a11a-3a05-4a6a-84c1-38b69157e730
Path_to_Pneumonia = "C:/Users/mpeswadmin/Downloads/chest_xray/train/PNEUMONIA/"

# ╔═╡ ee58175d-697a-4301-92e7-c8d30cb68bc4
images = readdir(Path_to_Pneumonia)

# ╔═╡ 5e99d544-8ef6-4803-88f7-07fc461a7ecc


# ╔═╡ eca0cbeb-ac9c-47f7-9730-219d55dfc2ef


# ╔═╡ Cell order:
# ╟─7ba7cb1e-cf66-11eb-1b2e-51f8c0053504
# ╟─fd565058-033c-4f97-85c9-ac48bf4d4dcb
# ╠═15f8b5cb-ffc3-4520-b3d1-6f2d8d687f81
# ╠═5d060036-20d8-4eb5-b2f0-82d3afbf1433
# ╠═84757d04-ac8c-4f49-9304-0eff91a71ee6
# ╠═aeb62711-882c-4965-9e69-ea0f16f137a8
# ╠═a59ac236-0851-4824-a403-7b5484320975
# ╠═1b2d7df8-9f85-4d9e-a83c-b2eabed0be9b
# ╠═4bc3f7eb-68b9-466a-b348-1787cba409fd
# ╠═32248514-0f6d-4dba-ad09-715ab8a1d34c
# ╠═167dab5f-c7b5-4048-b773-ccd55eaa0a9e
# ╠═4e7da1ce-2fe2-4f46-8adc-0fd5af961c02
# ╠═2b175cd8-a5f9-47e8-86d9-92a0a91bbec1
# ╠═a2a4301d-e54b-43a2-ab46-a1bfde2b6008
# ╠═61b84f5c-64cd-463a-8d2c-e28dd9c7629f
# ╠═8eaef5a1-59ab-4fe4-8dd6-961582346155
# ╠═b3216138-30be-4ca7-8d22-0430dbba195b
# ╠═6dd0a5ad-3e11-4cf9-b17d-c2ebed5b669a
# ╠═adc0d715-3d6d-4bc3-97e2-938ec2aed640
# ╠═29fb15c9-38fb-4c29-b035-2b44bf8a86ad
# ╠═d780b2f1-2403-4af6-a529-272179a22d89
# ╠═386755fc-61c4-4af4-b5d1-c027f2986b9d
# ╠═e91d905e-fc5c-46b2-a2ca-8c6726345b9f
# ╠═e5d366fb-30d4-409a-8b6a-210f95f9e627
# ╠═77774475-2edf-47ad-b3ed-11ea6c25a2f2
# ╠═d388cae5-60c2-4c30-b1d7-afcc28d53952
# ╠═140619df-cd75-48bd-8bf1-01eab51e1a6e
# ╠═e3d7a8ce-8dac-45ea-a65f-82bd1dcc3133
# ╠═7ced223b-d395-40f9-9c16-7d3df23213fe
# ╠═576af1ce-70a1-4e9f-acdc-786dad1dbd92
# ╠═fddff671-ec45-43c3-9c31-4a7f7817f305
# ╠═53550261-e47d-4582-a1ba-e389895ed929
# ╠═7c46d9ba-7866-44bf-b1bc-7d9e71941a63
# ╠═df0f6347-c4f2-441a-8c38-e71b856eae15
# ╠═8d880c29-f705-46cd-a32f-99efa51ad630
# ╠═3a6a4ce5-78a9-46df-b083-cabae8787922
# ╠═ef9eb368-b92e-4950-8443-3360ddd48a94
# ╠═862d14f9-c307-4164-b594-1f7e59cd17ac
# ╠═621a03ff-0dba-4072-86db-bd492626c758
# ╠═d5e9a11a-3a05-4a6a-84c1-38b69157e730
# ╠═ee58175d-697a-4301-92e7-c8d30cb68bc4
# ╠═5e99d544-8ef6-4803-88f7-07fc461a7ecc
# ╠═eca0cbeb-ac9c-47f7-9730-219d55dfc2ef
